package habra.examples;

public class People {
    Integer age;
    String sex;

    public People(Integer age, String sex) {
        this.age = age;
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "People{" +
                "age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
