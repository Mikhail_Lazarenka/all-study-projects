package habra.examples;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _2WorkWithStreams {
    public static void main(String[] args) {
        Collection<String> collection = Arrays.asList("a1", "a2", "a3","a4", "a5", "a4","a4");
//        1. filter
        long count = collection.stream().filter("a4"::equals).count();
//        System.out.println(count); //3

//        2. skip
        List<String> collect4 = collection.stream().skip(collection.size() - 5).collect(Collectors.toList());
        System.out.println(collect4); //[a3, a4, a5, a4, a4]

//        3. distinct
        List<String> collect = collection.stream().distinct().collect(Collectors.toList());
//        System.out.println(collect); // [a1, a2, a3, a4, a5]

//        4. map
        List<String> collect1 = collection.stream().map(a -> a + "_").collect(Collectors.toList());
//        System.out.println(collect1); //[a1_, a2_, a3_, a4_, a5_, a4_, a4_]

//        5. peek apply a function to the each element
//        List<String> collect2 = collection.stream().map(e -> e.toUpperCase()).peek(System.out::println).collect(Collectors.toList());

//        6. limit returns first n elements
        List<String> collect3 = collection.stream().limit(3).collect(Collectors.toList());
//        System.out.println(collect3); //[a1, a2, a3]
//        7. sorted
        List<String> collect2 = collection.stream().sorted().collect(Collectors.toList());
//        System.out.println(collect2); //[a1, a2, a3]

//        8. mapToInt mapToDouble mapToLong
//        List<String> strings = Arrays.asList("1", "2");
//        int[] ints = strings.stream().mapToInt(e -> Integer.parseInt(e)).toArray();
////        System.out.println(ints.toString());
////        9. flatMap flatMapToInt flatMapToDouble flatMapToLong
//        Похоже на map, но может создавать из одного элемента несколько
//        String[] strings1 = collection.stream().flatMap((p) -> Arrays.asList(p.split(",")).stream()).toArray(String[]::new);
//        System.out.println(strings1.toString());
    }
}
