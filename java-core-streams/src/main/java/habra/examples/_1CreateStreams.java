package habra.examples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//https://habr.com/ru/company/luxoft/blog/270383/
public class _1CreateStreams {
    public static void main(String[] args) {
//        1. classical
        Collection<String> collection = Arrays.asList("a1", "a2", "a3");
        Stream<String> streamFromCollection = collection.stream();
//        2. from values
        Stream<String> streamFromValues = Stream.of("a1", "a2", "a3");
//        from arrays
        String[] array = {"a1", "a2", "a3"};
        Stream<String> streamFromArrays = Arrays.stream(array);
//        3. from file
        try {
            Stream<String> streamFromFiles = Files.lines(Paths.get("file.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }


//        4. from string
        IntStream intStream = "123".chars();
//        5. by using stream builder
        Stream.builder().add("a1").add("a2").build();
//        6. create infinity stream
        Stream<Integer> iterate = Stream.iterate(1, n -> n + 1);
//        7. creating infinity steam by using generate
        Stream<String> streamFromGenerate = Stream.generate(() -> "a1");



    }

}
