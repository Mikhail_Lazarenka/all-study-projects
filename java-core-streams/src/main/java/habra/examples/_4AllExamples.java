package habra.examples;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class _4AllExamples {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("a1", "a2", "a3", "a1");
//        веркнуть количество вхождение а1
        long count = strings.stream().filter("a1"::equals).count();
//        System.out.println(count); // 2
//        Вернуть первый элемент коллекции или 0, если коллекция пуста
        strings.stream().findFirst().orElse("0");
//        Вернуть последний элемент коллекции или «empty», если коллекция пуста
        strings.stream().skip(strings.size()-1).findAny().orElse("empty");
//        Найти элемент в коллекции равный «a3» или кинуть ошибку
        strings.stream().filter("a3"::equals).findFirst().get();
//        Вернуть третий элемент коллекции по порядку
        strings.stream().skip(2).findFirst().get();
//        Вернуть два элемента начиная со второго
        List<String> collect = strings.stream().skip(1).limit(2).collect(Collectors.toList());
//        System.out.println(collect);
//        Выбрать все элементы по шаблону
        strings.stream().filter(s -> s.contains("1")).collect(Collectors.toList());



//        more difficult examples
//        Выбрать мужчин-военнообязанных (от 18 до 27 лет)
        List<People> people = Arrays.asList(new People(21, "male"), new People(7, "female"), new People(16, "male"), new People(19, "male"));
        List<People> collect1 = people.stream().filter(p -> p.getAge() >= 18 && p.getAge() < 27 && "male".equals(p.getSex())).collect(Collectors.toList());
//        System.out.println(collect1);
//        Найти средний возраст среди мужчин
        double age = people.stream().filter(p -> p.getSex().equals("male")).mapToInt(People::getAge).average().getAsDouble();
//        System.out.println(age);
//        Найти кол-во потенциально работоспособных людей в выборке (т.е. от 18 лет и учитывая что женщины выходят в 55 лет, а мужчина в 60)

//        distinct
        List<String> aa = Arrays.asList( "a2", "a2", "a3", "a1", "a2", "a2");
        List<String> collect2 = aa.stream().distinct().collect(Collectors.toList());
//        System.out.println(collect2);
        Collection<String> nonOrdered = new HashSet<>(aa);
        List<String> collect3 = nonOrdered.stream().unordered().distinct().collect(Collectors.toList());
//        System.out.println(collect3);
        List<Integer> collect4 = aa.stream().map(s -> Integer.parseInt(s.substring(1))).collect(Collectors.toList());
//        System.out.println(collect4);

//        Примеры использования Map функций (map, mapToInt, FlatMap, FlatMapToInt)
//        Условие: даны две коллекции collection1 = Arrays.asList(«a1», «a2», «a3», «a1») и collection2 = Arrays.asList(«1,2,0», «4,5»),
//        давайте посмотрим как её можно обрабатывать используя различные map функции

        List<String> collection1 = Arrays.asList("a1", "a2", "a3", "a1");
        List<String> collection2 = Arrays.asList("1,2,0", "4,5");
//        Добавить "_1" к каждому элементу первой коллекции
//        List<String> string = collection1.stream().map(a -> a + "_1").collect(Collectors.toList());
//        System.out.println(string); //
//        В первой коллекции убрать первый символ и вернуть массив чисел (int[])
//        int[] ints = collection2.stream().mapToInt( a -> Integer.parseInt(a.substring(1))).toArray();
//        Arrays.stream(ints).forEach(System.out::print);
//        Из второй коллекции получить все числа, перечисленные через запятую из всех элементов

//        String[] stringArg = collection2.stream().flatMap(c -> Arrays.stream(c.split(","))).toArray(String[]::new);
//        String[] strings1 = collection2.stream().flatMap((p) -> Arrays.asList(p.split(",")).stream()).toArray(String[]::new);
//        Arrays.stream(strings1).forEach(System.out::print);
//        Из второй коллекции получить сумму всех чисел, перечисленных через запятую
//        int sum = collection2.stream().flatMapToInt((p) -> Arrays.asList(p.split(",")).stream().mapToInt(Integer::parseInt)).sum();
//        System.out.println(sum);

//        Примеры использования Sorted функции
//        Отсортировать коллекцию строк по алфавиту
//        Условие: даны две коллекции коллекция строк Arrays.asList("a1", "a4", "a3", "a2", "a1", "a4")
//        и коллекция людей класса People (с полями name — имя, age — возраст, sex — пол),
//        вида Arrays.asList( new People("Вася", 16, Sex.MAN), new People("Петя", 23, Sex.MAN),
//        new People("Елена", 42, Sex.WOMEN), new People("Иван Иванович", 69, Sex.MAN)).
//        Давайте посмотрим примеры как их можно сортировать:

//        Отсортировать коллекцию строк по алфавиту в обратном порядке
        List<String> strings1 = Arrays.asList("a1", "a4", "a3", "a2", "a1", "a4");
        strings1.stream().sorted((o1, o2) -> o1.compareTo(o2)).collect(Collectors.toList());
//        Отсортировать коллекцию строк по алфавиту и убрать дубликаты
        List<String> collect5 = strings1.stream().distinct().sorted().collect(Collectors.toList());
//        System.out.println(collect5); //[a1, a2, a3, a4]
//        Отсортировать коллекцию строк по алфавиту в обратном порядке и убрать дубликаты
//        Отсортировать коллекцию людей по имени в обратном алфавитном порядке
        List<People2> people2s = Arrays.asList(new People2("а", 16, Sex.MAN),
                new People2("б", 23, Sex.MAN),
                new People2("в", 42, Sex.WOMEN),
                new People2("г Иванович", 69, Sex.MAN));
        List<People2> collect6 = people2s.stream().sorted((p1, p2) -> p2.getName().compareTo(p1.getName())).collect(Collectors.toList());
        System.out.println(collect6);
//        Отсортировать коллекцию людей сначала по полу, а потом по возрасту
        List<People2> collect7 = people2s.stream().sorted((p1, p2) -> p1.getSex() != p2.getSex() ?
                p1.getSex().compareTo(p2.getSex()) : p1.getAge().compareTo(p2.getAge())).collect(Collectors.toList());


//        Примеры использования Max и Min функций
//        Найти максимальное значение среди коллекции строк
//        Найти минимальное значение среди коллекции строк
//        Найдем человека с максимальным возрастом
//        Найдем человека с минимальным возрастом


//        Примеры использования ForEach и Peek функций

//        Примеры использования Reduce функции
//        Получить сумму чисел или вернуть 0
//        Вернуть максимум или -1
//        Вернуть сумму нечетных чисел или 0

//        Примеры использования toArray и collect функции
//        Получить сумму нечетных чисел
//        Вычесть от каждого элемента 1 и получить среднее
//        Прибавить к числам 3 и получить статистику
//        Разделить числа на четные и нечетные
//        Получение списка без дубликатов
//        Получить массив строк без дубликатов и в верхнем регистре
//        Объединить все элементы в одну строку через разделитель: и обернуть тегами <b>… </b>
//        Преобразовать в map, где первый символ ключ, второй символ значение
//        Преобразовать в map, сгруппировав по первому символу строки
//        Преобразовать в map, сгруппировав по первому символу строки и объединим вторые символы через :

//        Пример создания собственного Collector'a

    }
}
