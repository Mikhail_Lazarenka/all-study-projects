package habra.examples;

import java.util.Arrays;
import java.util.Collection;

public class _3TerminalExamples {
    public static void main(String[] args) {
        Collection<String> collection = Arrays.asList("a1", "a2", "a3","a4", "a5", "a4","a4");
//        1. findFirst
        String the_collection_is_empty = collection.stream().findFirst().orElse("the collection is empty");
//        System.out.println(the_collection_is_empty);
//        2. findAny
        String s = collection.stream().findAny().orElse("1");
//        System.out.println(s); //a1
//        3. collect
//        4. count
        long a = collection.stream().filter("a4"::equals).count();
//        System.out.println(a); // 3
//        5. anyMatch
        boolean b = collection.stream().anyMatch("a7"::equals); // false
//        System.out.println(b);
//        6. nonMatch
        boolean c = collection.stream().noneMatch("a9"::equals);
//        System.out.println(c); // true
//        7. allMatch
        boolean n = collection.stream().allMatch( h -> h.contains("a"));
//        System.out.println(n); // true
//        8. min
        String g = collection.stream().min(String::compareTo).get();
//        System.out.println(g); //a1
//        9. max
//        10 forEach without order
//        collection.stream().forEach(System.out::printf);
//        11. forEachOrdered with order
//        12. toArray
        String[] strings = collection.stream().map(String::toUpperCase).toArray(String[]::new);
//        Arrays.stream(strings).forEach(System.out::println);
//        13. reduce let you to use  functions
        String s3 = collection.stream().reduce((s1, s2) -> s1 + s2).get();
//        System.out.println(s3);  //a1a2a3a4a5a4a4
    }
}
