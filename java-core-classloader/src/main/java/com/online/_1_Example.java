package com.online;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

//http://java-online.ru/java-interview-05.xhtml
public class _1_Example {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
//        Определение свойств класса в режиме run-time
//        с использование рефлексии
        Class<?> foo = Class.forName("com.online.Foo");
//        получаем модификатор доступа класса
        int modifiers = foo.getModifiers();
        if (Modifier.isPublic(modifiers))	{ System.out.println("public");  }
        if (Modifier.isAbstract(modifiers))	{ System.out.println("abstract");}
        if (Modifier.isFinal(modifiers))	{ System.out.println("final");   }
//        получаем супер класс
        Class<?> superclass = foo.getSuperclass();
        System.out.println(superclass.toString());
//        список всех интерфейсов
        Class<ArrayList> arrayListClass = ArrayList.class;
        Class<?>[] interfaces = arrayListClass.getInterfaces();
        for (Class<?> interf: interfaces){
            System.out.println(interf.getName());
        }
// getField & getFields возвращают олько открытые поля поэтому для того чтобы вернуть все надо юзать getDeclaredField{s}()
        Class<?> cls = Class.forName("com.online.Foo");
        Foo foo1 = new Foo("privateString", "publicString", "protectedString");
        Field[] fields = cls.getFields();
        for (Field field: fields){
            Class<?> type = field.getType();
            String name = field.getName();
            System.out.println("type: "+ type + " \nname: "+name); //только публичные type: class java.lang.String name: publicString
        }
//        Field privateString = cls.getField("privateString");
//        getDeclaredFields
        Field privateString1 = cls.getDeclaredField("privateString");
        privateString1.setAccessible(true);
        Object o = privateString1.get(foo1);
        System.out.println(o.toString()); // т.е в чем прикол чтобы получить какое-то значение
        // в ран тайме ты должен иметь две вещи: это объект класса класс и экземпляр класса
// getMethod{s} но тут уже надо указать не только имя метода  но и параметры
        Class<?> cls3 = foo.getClass();
        Class[] params = new Class[] {Integer.class, String.class};

//        Method method = cls3.getMethod("methodName", params);

//       вызов метода
        Class<Foo> fooClass = Foo.class; // нам надо о класса класс
        Class[] params2 = {int.class, String.class}; // подготавливаем параметры для получения метода из о клааса класс
        Method m1 = fooClass.getDeclaredMethod("m1", params2); // достаём метод
        m1.setAccessible(true);
        Object[] args2 = {(int) 123, new String("mike")}; // подготавливаем аргументы для вызова метода
        Object invoke = m1.invoke(new Foo(), args2); // вызываем метод!!



    }
}
