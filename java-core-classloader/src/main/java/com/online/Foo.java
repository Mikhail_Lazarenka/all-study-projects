package com.online;

public class Foo {
    private String privateString;
    public String publicString;
    protected String protectedString;

    public Foo() {
    }

    public Foo(String privateString, String publicString, String protectedString) {
        this.privateString = privateString;
        this.publicString = publicString;
        this.protectedString = protectedString;
    }

    public void m1(int arg1, String arg2){
        System.out.println(arg1+arg2);
    }
    private void m2(){
        System.out.println("m2");
    }
}
