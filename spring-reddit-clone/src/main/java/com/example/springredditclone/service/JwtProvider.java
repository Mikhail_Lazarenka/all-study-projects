package com.example.springredditclone.service;


import com.example.springredditclone.exception.SpringRedditException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.UUID;

import static io.jsonwebtoken.Jwts.parser;
import static java.util.Date.from;

@Service
public class JwtProvider {

    private KeyStore keyStore;
    @Value("${jwt.expiration.time}")
    private Long jwtExpirationInMillis;
    @Value("${jwt.secret}")
    private String secret;

    private Key hmacKey ;


    @PostConstruct
    public void init() {
       hmacKey =  new SecretKeySpec(Base64.getDecoder().decode(secret),
                SignatureAlgorithm.HS256.getJcaName());

    }
    public String generateToken(Authentication authentication) {
        Instant now = Instant.now();
        User user = (User) authentication.getPrincipal();
        return  Jwts.builder()
                .setSubject(user.getUsername())
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(5l, ChronoUnit.MINUTES)))
                .signWith(hmacKey)
                .compact();
    }

    public String generateTokenWithUserName(String username) {
        return  Jwts.builder()
                .setSubject(username)
                .setId(UUID.randomUUID().toString())
                .signWith(hmacKey)
                .compact();
    }



    public boolean validateToken(String jwt) {
        parser().setSigningKey(hmacKey).parseClaimsJws(jwt);
        return true;
    }


    public String getUsernameFromJwt(String token) {
        Claims claims = parser()
                .setSigningKey(hmacKey)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

}