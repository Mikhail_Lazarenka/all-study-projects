package _1_strategy;

import _1_strategy.headfirst.algorithms.CoolQuack;
import _1_strategy.headfirst.algorithms.RocketFly;

public class RocketDuck extends Duck {
    @Override
    public void display() {
        System.out.println("Rocket duck");
        performFly();
        performQuack();
    }
    public RocketDuck(){
        this.quackAbility = new CoolQuack();
        this.flyAbility = new RocketFly();
    }
}
