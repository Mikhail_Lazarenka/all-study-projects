package _1_strategy.guru;

import java.math.BigDecimal;

public class CreditCardStrategy implements PaymentStrategy{
    @Override
    public void pay(BigDecimal amount) {
        System.out.println("Pay by card");
    }
}
