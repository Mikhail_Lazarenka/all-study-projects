package _1_strategy.guru;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class Order {

    private BigDecimal amount;

    public void process(final PaymentStrategy strategy) {
        strategy.pay(amount);
    }
}
