package _1_strategy.guru;

import java.math.BigDecimal;

public class PayPalStrategy implements PaymentStrategy{
    @Override
    public void pay(BigDecimal amount) {
        System.out.println("PayPal strategy");
    }
}
