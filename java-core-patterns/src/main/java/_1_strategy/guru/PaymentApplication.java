package _1_strategy.guru;

import java.math.BigDecimal;

//https://refactoring.guru/ru/design-patterns/strategy

public class PaymentApplication {
    public static void main(String[] args) {
        var order = new Order();
        order.setAmount(BigDecimal.ONE);
        order.process(new PayPalStrategy());
    }
}
