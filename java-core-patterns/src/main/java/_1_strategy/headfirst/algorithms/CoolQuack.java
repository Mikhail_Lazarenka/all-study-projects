package _1_strategy.headfirst.algorithms;

public class CoolQuack implements QuackAbility {
    @Override
    public void quack() {
        System.out.println("Cool quack");
    }

}