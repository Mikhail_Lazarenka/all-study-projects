package _1_strategy.headfirst.algorithms;

public class SimpleFly implements FlyAbility{
    @Override
    public void fly() {
        System.out.println("Simple Fly");
    }

}
