package _1_strategy.headfirst.algorithms;

public class SimpleQuack implements QuackAbility{
    @Override
    public void quack() {
        System.out.println("Simple quack");
    }
}
