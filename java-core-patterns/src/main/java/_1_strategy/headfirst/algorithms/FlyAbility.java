package _1_strategy.headfirst.algorithms;

public interface FlyAbility {
    void fly();
}
