package _1_strategy.headfirst.algorithms;

public interface QuackAbility {
    void quack();
}
