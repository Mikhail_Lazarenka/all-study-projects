package _1_strategy.headfirst.algorithms;

public class RocketFly implements FlyAbility{
    @Override
    public void fly() {
        System.out.println("Rocket fly");
    }
}
