package _1_strategy;

import _1_strategy.headfirst.algorithms.FlyAbility;
import _1_strategy.headfirst.algorithms.QuackAbility;
import _1_strategy.headfirst.algorithms.SimpleFly;

public abstract class Duck {
    public FlyAbility flyAbility;
    public QuackAbility quackAbility;

    public abstract void display();

    public void performFly() {
        flyAbility.fly();
    }

    public void performQuack() {
        quackAbility.quack();
    }

    public void setFlyAbility(final FlyAbility flyAbility) {
        this.flyAbility = flyAbility;
    }

    public void setQuackAbility(final QuackAbility quackAbility) {
        this.quackAbility = quackAbility;
    }

    public static void main(String[] args) {
        var rocketDuck = new RocketDuck();
        rocketDuck.display();
        rocketDuck.setFlyAbility(new SimpleFly());
        rocketDuck.display();
    }

}
