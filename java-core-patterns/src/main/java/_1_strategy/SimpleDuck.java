package _1_strategy;

import _1_strategy.headfirst.algorithms.SimpleFly;
import _1_strategy.headfirst.algorithms.SimpleQuack;

public class SimpleDuck extends Duck {
    @Override
    public void display() {
        System.out.println("Simple duck");
        performFly();
        performQuack();
    }

    public SimpleDuck() {
        this.flyAbility = new SimpleFly();
        this.quackAbility = new SimpleQuack();
    }
}
