import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class MyList<T> {
    private Node<T> head;
    private Node<T> tail;

    public int size() {
        int size = 0;
        for (Node<T> current = tail; current != null; current = current.getNextValue()) {
            size++;
        }
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void add(final T newValue) {
        if (isEmpty()) {
            Node<T> node = new Node<>(newValue);
            this.head = node;
        } else {
            Node<T> node = new Node<>(newValue);
            var current = head;
            while (current.getNextValue() != null) {
                current = current.getNextValue();
            }
            current.setNextValue(node);
        }
    }


    private class Node<T> {
        private T value;
        private Node<T> nextValue;

        public T getValue() {
            return value;
        }

        public void setValue(final T value) {
            this.value = value;
        }

        public Node<T> getNextValue() {
            return nextValue;
        }

        public void setNextValue(final Node<T> nextValue) {
            this.nextValue = nextValue;
        }

        public Node(final T value) {
            this.value = value;
        }
    }

}
