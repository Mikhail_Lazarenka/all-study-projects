package _2_observer.headfirst.dipslay;

import _2_observer.headfirst.Display;
import _2_observer.headfirst.Observer;
import _2_observer.headfirst.Subject;
import _2_observer.headfirst.WeatherData;

public class AndroidDisplay implements Observer, Display {

    private WeatherData weatherData = new WeatherData();
    private Subject subject;

    public AndroidDisplay(final Subject subject) {
        this.subject = subject;
        subject.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Android display: " + weatherData.toString());
    }

    @Override
    public void update(WeatherData data) {
        this.weatherData = data;
        display();
    }
}
