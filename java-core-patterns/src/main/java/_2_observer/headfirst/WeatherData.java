package _2_observer.headfirst;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeatherData {
    private float temperature;
    private float humidity;
    private float pressure;

    @Override
    public String toString() {
        return "WeatherData{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", pressure=" + pressure +
                '}';
    }
}
