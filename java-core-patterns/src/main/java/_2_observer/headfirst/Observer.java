package _2_observer.headfirst;

public interface Observer {
    void update(final WeatherData data);
}
