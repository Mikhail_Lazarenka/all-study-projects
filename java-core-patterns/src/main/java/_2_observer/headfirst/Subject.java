package _2_observer.headfirst;

public interface Subject {
    void registerObserver(final Observer observer);
    void removeObserver(final Observer observer);
    void notifyAllObservers();
}
