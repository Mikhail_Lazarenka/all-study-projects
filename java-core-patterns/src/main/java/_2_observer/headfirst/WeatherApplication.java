package _2_observer.headfirst;

import _2_observer.headfirst.dipslay.AndroidDisplay;
import _2_observer.headfirst.dipslay.IphoneDisplay;

public class WeatherApplication {
    public static void main(String[] args) {
        var weatherData = new WeatherData();
        weatherData.setHumidity(1f);
        weatherData.setPressure(1f);
        weatherData.setTemperature(1f);

        var weatherStation = new WeatherStation();
        weatherStation.setWeatherData(weatherData);

        var iphoneDisplay = new IphoneDisplay(weatherStation);
        var androidDisplay = new AndroidDisplay(weatherStation);

        var weatherData1 = new WeatherData();
        weatherData1.setHumidity(2f);
        weatherData1.setPressure(2f);
        weatherData1.setTemperature(2f);
        weatherStation.setWeatherData(weatherData1);

    }
}
