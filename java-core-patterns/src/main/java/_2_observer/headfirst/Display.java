package _2_observer.headfirst;

public interface Display {
    void display();
}
