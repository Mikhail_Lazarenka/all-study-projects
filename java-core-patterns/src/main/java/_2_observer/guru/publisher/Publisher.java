package _2_observer.guru.publisher;

import _2_observer.guru.listener.Listener;

public interface Publisher {
    void subscribe(final String eventType, final Listener listener);
    void unsubscribe(final String eventType, final Listener listener);
    void notifyListeners(final String eventType, final String fileName);
}
