package _2_observer.guru.publisher;

import _2_observer.guru.listener.Listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManager implements Publisher {

    private Map<String, List<Listener>> listeners = new HashMap<>();

    public EventManager(String... eventTypes) {
        Arrays.stream(eventTypes).forEach(eventType-> listeners.put(eventType, new ArrayList<>()));
    }

    @Override
    public void subscribe(String eventType, Listener listener) {
        listeners.get(eventType).add(listener);
    }

    @Override
    public void unsubscribe(String eventType, Listener listener) {
        listeners.get(eventType).remove(listener);
    }

    @Override
    public void notifyListeners(final String eventType, final String fileName) {
        listeners.get(eventType).forEach(listener -> listener.update(eventType, fileName));
    }
}
