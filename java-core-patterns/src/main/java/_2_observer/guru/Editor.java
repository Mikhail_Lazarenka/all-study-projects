package _2_observer.guru;

import _2_observer.guru.publisher.EventManager;
import lombok.SneakyThrows;

public class Editor {
    private final EventManager eventManager;
    private String fileName;

    public Editor() {
        this.eventManager = new EventManager("open", "save");
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public void openFile(final String fileName) {
        this.fileName = fileName;
        eventManager.notifyListeners("open", fileName);
    }

    @SneakyThrows
    public void saveFile() {
        if (this.fileName != null) {
            eventManager.notifyListeners("save", fileName);
        } else {
            throw new Exception("Please open a file first.");
        }
    }
}
