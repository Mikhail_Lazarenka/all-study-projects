package _2_observer.guru;

import _2_observer.guru.listener.EmailListener;
import _2_observer.guru.listener.LogOpenListener;

//https://refactoring.guru/ru/design-patterns/observer/java/example#example-0--editor-Editor-java

public class EditorDemo {
    public static void main(String[] args) {
        var editor = new Editor();
        editor.getEventManager().subscribe("open", new LogOpenListener());
        editor.getEventManager().subscribe("save", new EmailListener());
        editor.openFile("mikeFile.cool");
        editor.saveFile();
    }
}
