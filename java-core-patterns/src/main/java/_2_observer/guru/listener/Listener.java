package _2_observer.guru.listener;

public interface Listener {
    void update(String eventType, String fileName);
}
