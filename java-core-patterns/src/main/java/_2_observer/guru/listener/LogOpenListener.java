package _2_observer.guru.listener;

public class LogOpenListener implements Listener {
    @Override
    public void update(String eventType, String fileName) {
        System.out.println("Log open listener " + eventType + " " + fileName);
    }
}
