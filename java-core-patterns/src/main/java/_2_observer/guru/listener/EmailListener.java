package _2_observer.guru.listener;

public class EmailListener implements Listener {
    @Override
    public void update(String eventType, String fileName) {
        System.out.println("Email listener " + eventType + " " + fileName);
    }
}
