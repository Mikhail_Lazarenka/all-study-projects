package _5_abstract_factory.headfirst.pizza.ingredient;

import _5_abstract_factory.headfirst.pizza.Descriable;

public interface Cheese extends Descriable {
}
