package _5_abstract_factory.headfirst.pizza.ingredient.ny;

import _5_abstract_factory.headfirst.pizza.ingredient.Cheese;

public class NYCheese implements Cheese {
    @Override
    public void describe() {
        System.out.println("NY cheese");
    }
}
