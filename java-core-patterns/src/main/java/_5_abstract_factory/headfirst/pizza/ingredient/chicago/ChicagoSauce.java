package _5_abstract_factory.headfirst.pizza.ingredient.chicago;

import _5_abstract_factory.headfirst.pizza.ingredient.Sauce;

public class ChicagoSauce implements Sauce {
    @Override
    public void describe() {
        System.out.println("Chicago Sauce");
    }
}
