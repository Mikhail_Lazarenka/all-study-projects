package _5_abstract_factory.headfirst.pizza.ingredient.factory;

import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.pizza.ingredient.Cheese;
import _5_abstract_factory.headfirst.pizza.ingredient.Sauce;
import _5_abstract_factory.headfirst.pizza.ingredient.ny.NYCheese;
import _5_abstract_factory.headfirst.pizza.ingredient.ny.NYSauce;

public class NYIngredientFactory extends AbstractIngredientFactory{
    @Override
    public DoughType getDoughType() {
        return DoughType.THICK;
    }

    @Override
    public Cheese getCheese() {
        return new NYCheese();
    }

    @Override
    public Sauce getSauce() {
        return new NYSauce();
    }
}
