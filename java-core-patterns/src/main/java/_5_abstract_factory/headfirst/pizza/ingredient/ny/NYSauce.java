package _5_abstract_factory.headfirst.pizza.ingredient.ny;

import _5_abstract_factory.headfirst.pizza.ingredient.Sauce;

public class NYSauce implements Sauce {
    @Override
    public void describe() {
        System.out.println("NY Sauce");
    }
}
