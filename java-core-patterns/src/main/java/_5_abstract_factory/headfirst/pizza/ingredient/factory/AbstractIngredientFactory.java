package _5_abstract_factory.headfirst.pizza.ingredient.factory;

import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.pizza.ingredient.Cheese;
import _5_abstract_factory.headfirst.pizza.ingredient.Sauce;

public abstract class AbstractIngredientFactory {
    public abstract DoughType getDoughType();
    public abstract Cheese getCheese();
    public abstract Sauce getSauce();

}
