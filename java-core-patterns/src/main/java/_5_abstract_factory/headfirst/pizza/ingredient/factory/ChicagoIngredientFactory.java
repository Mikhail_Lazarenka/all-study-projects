package _5_abstract_factory.headfirst.pizza.ingredient.factory;

import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.pizza.ingredient.Cheese;
import _5_abstract_factory.headfirst.pizza.ingredient.Sauce;
import _5_abstract_factory.headfirst.pizza.ingredient.chicago.ChicagoCheese;
import _5_abstract_factory.headfirst.pizza.ingredient.chicago.ChicagoSauce;

public class ChicagoIngredientFactory extends AbstractIngredientFactory{
    @Override
    public DoughType getDoughType() {
        return DoughType.THICK;
    }

    @Override
    public Cheese getCheese() {
        return new ChicagoCheese();
    }

    @Override
    public Sauce getSauce() {
        return new ChicagoSauce();
    }
}
