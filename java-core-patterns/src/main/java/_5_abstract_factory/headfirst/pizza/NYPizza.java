package _5_abstract_factory.headfirst.pizza;

import _5_abstract_factory.headfirst.pizza.ingredient.factory.AbstractIngredientFactory;

public class NYPizza extends Pizza {
    public NYPizza(AbstractIngredientFactory factory) {
        super(factory);
    }

    @Override
    public void describe(){
        System.out.println("--- NY Pizza ---");
        super.describe();
    }
}
