package _5_abstract_factory.headfirst.pizza;

import _5_abstract_factory.headfirst.pizza.ingredient.Cheese;
import _5_abstract_factory.headfirst.pizza.ingredient.Sauce;
import _5_abstract_factory.headfirst.pizza.ingredient.factory.AbstractIngredientFactory;
import lombok.AllArgsConstructor;

public abstract class Pizza implements Descriable {
    private String name;
    private DoughType doughType;
    private Cheese cheese;
    private Sauce sauce;

    public Pizza(AbstractIngredientFactory factory) {
        doughType = factory.getDoughType();
        cheese = factory.getCheese();
        sauce = factory.getSauce();
    }

    @Override
    public void describe() {
        System.out.println("Name: " + name + ", Dough type: " + doughType + ", Cheese: " + cheese + ", Sauce: " + sauce);
    }
}
