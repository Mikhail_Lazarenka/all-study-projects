package _5_abstract_factory.headfirst.pizza;

public enum DoughType {
    THICK,
    THIN
}
