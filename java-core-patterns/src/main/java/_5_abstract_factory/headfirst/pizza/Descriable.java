package _5_abstract_factory.headfirst.pizza;

public interface Descriable {
    void describe();
}
