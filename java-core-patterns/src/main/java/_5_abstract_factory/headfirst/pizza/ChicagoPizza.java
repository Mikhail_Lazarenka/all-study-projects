package _5_abstract_factory.headfirst.pizza;

import _5_abstract_factory.headfirst.pizza.ingredient.factory.AbstractIngredientFactory;

public class ChicagoPizza extends Pizza {

    public ChicagoPizza(AbstractIngredientFactory factory) {
        super(factory);
    }

    @Override
    public void describe() {
        System.out.println("--- Chicago Pizza ---");
        super.describe();
    }
}
