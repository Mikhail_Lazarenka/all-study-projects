package _5_abstract_factory.headfirst;

import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.store.NYPizzaStore;

public class PizzaStoreDemo {
    public static void main(String[] args) {
        var nyPizzaStore = new NYPizzaStore();
        var pizza = nyPizzaStore.orderPizza(DoughType.THICK);
        pizza.describe();
    }
}
