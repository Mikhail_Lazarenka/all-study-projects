package _5_abstract_factory.headfirst.store;

import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.pizza.Pizza;

public abstract class PizzaStore {
    protected abstract Pizza bakePizza(DoughType doughType);

    public Pizza orderPizza(DoughType doughType){
        var pizza = bakePizza(doughType);
//        do something with pizza
        return pizza;
    }

}
