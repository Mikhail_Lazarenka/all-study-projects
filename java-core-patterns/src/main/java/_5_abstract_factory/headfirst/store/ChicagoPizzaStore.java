package _5_abstract_factory.headfirst.store;

import _5_abstract_factory.headfirst.pizza.ChicagoPizza;
import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.pizza.Pizza;
import _5_abstract_factory.headfirst.pizza.ingredient.chicago.ChicagoCheese;
import _5_abstract_factory.headfirst.pizza.ingredient.chicago.ChicagoSauce;
import _5_abstract_factory.headfirst.pizza.ingredient.factory.ChicagoIngredientFactory;

public class ChicagoPizzaStore extends PizzaStore {
    @Override
    protected Pizza bakePizza(DoughType doughType) {
        return new ChicagoPizza(new ChicagoIngredientFactory());
    }
}
