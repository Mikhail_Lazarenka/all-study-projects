package _5_abstract_factory.headfirst.store;

import _5_abstract_factory.headfirst.pizza.DoughType;
import _5_abstract_factory.headfirst.pizza.NYPizza;
import _5_abstract_factory.headfirst.pizza.Pizza;
import _5_abstract_factory.headfirst.pizza.ingredient.factory.NYIngredientFactory;
import _5_abstract_factory.headfirst.pizza.ingredient.ny.NYCheese;
import _5_abstract_factory.headfirst.pizza.ingredient.ny.NYSauce;

public class NYPizzaStore extends PizzaStore {

    @Override
    protected Pizza bakePizza(DoughType doughType) {
        return new NYPizza(new NYIngredientFactory());
    }
}
