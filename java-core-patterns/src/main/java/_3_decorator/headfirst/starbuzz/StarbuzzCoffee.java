package _3_decorator.headfirst.starbuzz;

import _3_decorator.headfirst.starbuzz.decorable.Beverage;
import _3_decorator.headfirst.starbuzz.decorable.Espresso;
import _3_decorator.headfirst.starbuzz.decorator.Mocha;
import _3_decorator.headfirst.starbuzz.decorator.Whip;

public class StarbuzzCoffee {
    public static void main(String[] args) {
        Beverage beverage = new Espresso(Beverage.Size.VENTI);
        beverage = new Mocha(beverage);
        beverage = new Whip(beverage);
        System.out.println(beverage.getDescription());
        System.out.println(beverage.cost());

    }
}
