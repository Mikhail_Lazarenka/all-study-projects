package _3_decorator.headfirst.starbuzz.decorator;

import _3_decorator.headfirst.starbuzz.decorable.Beverage;

import java.math.BigDecimal;

public class Mocha extends CondimentDecorator {

    public Mocha(Beverage beverage) {
        super(beverage);
    }

    @Override
    public BigDecimal cost() {
        return BigDecimal.valueOf(0.20).add(beverage.cost());
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Mocha";
    }
}
