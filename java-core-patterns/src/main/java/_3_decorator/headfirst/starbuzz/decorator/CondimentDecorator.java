package _3_decorator.headfirst.starbuzz.decorator;

import _3_decorator.headfirst.starbuzz.decorable.Beverage;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class CondimentDecorator extends Beverage {

    protected Beverage beverage;

    public abstract String getDescription();
}
