package _3_decorator.headfirst.starbuzz.decorable;

import java.math.BigDecimal;

public class Espresso extends Beverage {

    public Espresso(){
        this.description = "Espresso";
    }

    public Espresso(final Size size) {
        this();
        this.size = size;
    }

    @Override
    public BigDecimal cost() {
        return this.size.getCost().add(BigDecimal.valueOf(0.99));
    }
}
