package _3_decorator.headfirst.starbuzz.decorable;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

import static _3_decorator.headfirst.starbuzz.decorable.Beverage.Size.TALL;

public abstract class Beverage {
    protected String description = "Unknown";
    protected Size size = TALL;

    public String getDescription(){
        return size.name() + " " + description;
    }
    public abstract BigDecimal cost();

    @Getter
    @AllArgsConstructor
    public enum Size {
        TALL(BigDecimal.ONE), GRANDE(BigDecimal.valueOf(2)), VENTI(BigDecimal.valueOf(3));
        private final BigDecimal cost;
    }
}
