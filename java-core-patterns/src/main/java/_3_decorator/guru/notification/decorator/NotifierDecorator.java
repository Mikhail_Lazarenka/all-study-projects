package _3_decorator.guru.notification.decorator;

import _3_decorator.guru.notification.notifier.PhoneNotifier;
import _3_decorator.guru.notification.notifier.Notifier;

public abstract class NotifierDecorator implements Notifier {

    private final Notifier wrappedNotifier;

    public NotifierDecorator() {
        this.wrappedNotifier = new PhoneNotifier();
    }

    public NotifierDecorator(final Notifier notifier) {
        this.wrappedNotifier = notifier;
    }
    @Override
    public void send(final String message) {
        wrappedNotifier.send(message);
    }
}
