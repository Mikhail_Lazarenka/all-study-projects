package _3_decorator.guru.notification.decorator;

import _3_decorator.guru.notification.notifier.Notifier;

public class SmsNotifierDecorator extends NotifierDecorator {
    public SmsNotifierDecorator(Notifier notifier) {
        super(notifier);
    }

    @Override
    public void send(String message) {
        super.send(message);
        System.out.println("Sms notifier: " + message);
    }
}
