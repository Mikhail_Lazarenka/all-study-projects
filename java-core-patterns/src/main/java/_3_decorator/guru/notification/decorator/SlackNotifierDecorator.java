package _3_decorator.guru.notification.decorator;

import _3_decorator.guru.notification.notifier.Notifier;

public class SlackNotifierDecorator extends NotifierDecorator {

    public SlackNotifierDecorator(Notifier notifier) {
        super(notifier);
    }

    @Override
    public void send(String message) {
        super.send(message);
        System.out.println("Slack notifier: " + message);
    }
}
