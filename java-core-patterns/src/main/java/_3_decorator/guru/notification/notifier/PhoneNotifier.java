package _3_decorator.guru.notification.notifier;

public class PhoneNotifier implements Notifier{
    @Override
    public void send(String message) {
        System.out.println("Phone notifier: " + message);
    }
}
