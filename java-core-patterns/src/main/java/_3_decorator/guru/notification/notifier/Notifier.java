package _3_decorator.guru.notification.notifier;

public interface Notifier {
    void send(final String message);
}
