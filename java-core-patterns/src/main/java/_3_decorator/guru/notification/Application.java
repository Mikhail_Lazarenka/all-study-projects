package _3_decorator.guru.notification;

import _3_decorator.guru.notification.decorator.SlackNotifierDecorator;
import _3_decorator.guru.notification.decorator.SmsNotifierDecorator;
import _3_decorator.guru.notification.notifier.Notifier;
import _3_decorator.guru.notification.notifier.PhoneNotifier;

public class Application {
    public static void main(String[] args) {
        Notifier notifier = new PhoneNotifier();
        notifier = new SlackNotifierDecorator(notifier);
        notifier.send("hello");
        notifier = new SmsNotifierDecorator(notifier);
        notifier.send("test");
    }
}
