package _3_decorator.guru.datasource.decorator;

import _3_decorator.guru.datasource.decorable.DataSource;

public class EncryptionDataSourceDecorator extends DataSourceDecorator{

    public EncryptionDataSourceDecorator(DataSource wrapped) {
        super(wrapped);
    }

    @Override
    public void writeData(String data) {
        System.out.println("Decode data: " + data);
        super.writeData(data);

    }

    @Override
    public String readData() {
        var data = super.readData();
        System.out.println("Encode data: " + data);
        return data;
    }
}
