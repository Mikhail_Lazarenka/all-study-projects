package _3_decorator.guru.datasource.decorator;

import _3_decorator.guru.datasource.decorable.DataSource;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class DataSourceDecorator implements DataSource {
    private final DataSource wrapped;

    @Override
    public void writeData(String data) {
        wrapped.writeData(data);
    }

    @Override
    public String readData() {
        return wrapped.readData();
    }
}
