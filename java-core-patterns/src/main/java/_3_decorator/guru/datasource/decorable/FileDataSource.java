package _3_decorator.guru.datasource.decorable;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FileDataSource implements DataSource {

    private final String fileName;

    @Override
    public void writeData(String data) {
        System.out.println("File data source: WRITE data:" + data + " to file: " + fileName);
    }

    @Override
    public String readData() {
        System.out.println("File data source: READ from file: " + fileName);
        return "Readed";
    }
}
