package _3_decorator.guru.datasource.decorable;

public interface DataSource {
    void writeData(String data);

    String readData();
}
