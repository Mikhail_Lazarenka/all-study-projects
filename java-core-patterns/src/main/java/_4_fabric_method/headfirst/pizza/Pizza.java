package _4_fabric_method.headfirst.pizza;

import _4_fabric_method.headfirst.pizza.ingredient.Cheese;
import _4_fabric_method.headfirst.pizza.ingredient.Sauce;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Pizza implements Descriable {
    private String name;
    private DoughType doughType;
    private Cheese cheese;
    private Sauce sauce;

    @Override
    public void describe() {
        System.out.println("Name: " + name + ", Dough type: " + doughType + ", Cheese: " + cheese + ", Sauce: " + sauce);
    }
}
