package _4_fabric_method.headfirst.pizza;

public enum DoughType {
    THICK,
    THIN
}
