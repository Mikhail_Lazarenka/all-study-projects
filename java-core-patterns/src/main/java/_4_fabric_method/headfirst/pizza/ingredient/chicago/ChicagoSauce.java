package _4_fabric_method.headfirst.pizza.ingredient.chicago;

import _4_fabric_method.headfirst.pizza.ingredient.Sauce;

public class ChicagoSauce implements Sauce {
    @Override
    public void describe() {
        System.out.println("Chicago Sauce");
    }
}
