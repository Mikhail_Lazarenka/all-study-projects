package _4_fabric_method.headfirst.pizza.ingredient.ny;

import _4_fabric_method.headfirst.pizza.ingredient.Sauce;

public class NYSauce implements Sauce {
    @Override
    public void describe() {
        System.out.println("NY Sauce");
    }
}
