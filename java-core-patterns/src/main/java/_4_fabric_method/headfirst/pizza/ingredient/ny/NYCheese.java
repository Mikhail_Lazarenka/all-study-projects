package _4_fabric_method.headfirst.pizza.ingredient.ny;

import _4_fabric_method.headfirst.pizza.ingredient.Cheese;

public class NYCheese implements Cheese {
    @Override
    public void describe() {
        System.out.println("NY cheese");
    }
}
