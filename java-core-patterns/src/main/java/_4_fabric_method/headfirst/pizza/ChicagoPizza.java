package _4_fabric_method.headfirst.pizza;

import _4_fabric_method.headfirst.pizza.ingredient.Cheese;
import _4_fabric_method.headfirst.pizza.ingredient.Sauce;

public class ChicagoPizza extends Pizza{
    public ChicagoPizza(String name, DoughType doughType, Cheese cheese, Sauce sauce) {
        super(name, doughType, cheese, sauce);
    }

    @Override
    public void describe() {
        System.out.println("--- Chicago Pizza ---");
        super.describe();
    }
}
