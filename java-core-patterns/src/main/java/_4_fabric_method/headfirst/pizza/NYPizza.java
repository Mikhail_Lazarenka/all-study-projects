package _4_fabric_method.headfirst.pizza;

import _4_fabric_method.headfirst.pizza.ingredient.Cheese;
import _4_fabric_method.headfirst.pizza.ingredient.Sauce;

public class NYPizza extends Pizza{
    public NYPizza(String name, DoughType doughType, Cheese cheese, Sauce sauce) {
        super(name, doughType, cheese, sauce);
    }
    @Override
    public void describe(){
        System.out.println("--- NY Pizza ---");
        super.describe();
    }
}
