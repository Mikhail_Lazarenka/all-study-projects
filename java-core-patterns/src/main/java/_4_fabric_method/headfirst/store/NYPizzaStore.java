package _4_fabric_method.headfirst.store;

import _4_fabric_method.headfirst.pizza.DoughType;
import _4_fabric_method.headfirst.pizza.NYPizza;
import _4_fabric_method.headfirst.pizza.Pizza;
import _4_fabric_method.headfirst.pizza.ingredient.ny.NYCheese;
import _4_fabric_method.headfirst.pizza.ingredient.ny.NYSauce;

public class NYPizzaStore extends PizzaStore{

    @Override
    protected Pizza bakePizza(DoughType doughType) {
        return new NYPizza("NY Pizza", doughType, new NYCheese(), new NYSauce());
    }
}
