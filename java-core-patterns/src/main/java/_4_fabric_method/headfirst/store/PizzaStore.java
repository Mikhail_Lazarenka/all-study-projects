package _4_fabric_method.headfirst.store;

import _4_fabric_method.headfirst.pizza.DoughType;
import _4_fabric_method.headfirst.pizza.Pizza;

public abstract class PizzaStore {
    protected abstract Pizza bakePizza(DoughType doughType);

    public Pizza orderPizza(DoughType doughType){
        var pizza = bakePizza(doughType);
//        do something with pizza
        return pizza;
    }

}
