package _4_fabric_method.headfirst.store;

import _4_fabric_method.headfirst.pizza.ChicagoPizza;
import _4_fabric_method.headfirst.pizza.DoughType;
import _4_fabric_method.headfirst.pizza.Pizza;
import _4_fabric_method.headfirst.pizza.ingredient.chicago.ChicagoCheese;
import _4_fabric_method.headfirst.pizza.ingredient.chicago.ChicagoSauce;

public class ChicagoPizzaStore extends PizzaStore{
    @Override
    protected Pizza bakePizza(DoughType doughType) {
        return new ChicagoPizza("Chicago Pizza", doughType, new ChicagoCheese(), new ChicagoSauce());
    }
}
