package _4_fabric_method.headfirst;

import _4_fabric_method.headfirst.pizza.DoughType;
import _4_fabric_method.headfirst.pizza.Pizza;
import _4_fabric_method.headfirst.store.NYPizzaStore;

public class PizzaStoreDemo {
    public static void main(String[] args) {
        var nyPizzaStore = new NYPizzaStore();
        var pizza = nyPizzaStore.orderPizza(DoughType.THICK);
        pizza.describe();
    }
}
