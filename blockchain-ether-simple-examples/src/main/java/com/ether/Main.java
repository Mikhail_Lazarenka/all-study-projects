package com.ether;

import org.web3j.crypto.*;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Optional;

import static com.ether.Common.*;
//https://kauri.io/manage-an-ethereum-account-with-java-and-web3j/925d923e12c543da9a0a3e617be963b4/a
public class Main {

    public static void main(String[] args) throws IOException, CipherException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {

//        //how to see the balance
//        EthGetBalance balanceWei = web3.ethGetBalance(account1, DefaultBlockParameterName.LATEST).send();
//        System.out.println("balance in wei: " + balanceWei.getBalance());
//        BigDecimal balanceInEther = Convert.fromWei(balanceWei.getBalance().toString(), Convert.Unit.ETHER);
//        System.out.println("balance in ether: " + balanceInEther);
//
//        EthGetTransactionCount ethGetTransactionCount = web3.ethGetTransactionCount(account1, DefaultBlockParameterName.LATEST).send();
//        BigInteger nonce =  ethGetTransactionCount.getTransactionCount();
//        System.out.println(nonce);
//
//
//        // Load the JSON encryted wallet
//        Credentials credentials = WalletUtils.loadCredentials(walletPassword, walletDirectory1 + "/" + walletName1);
//        // Get the account address
//        String accountAddress = credentials.getAddress();
//        // Get the unencrypted private key into hexadecimal
//        String privateKey = credentials.getEcKeyPair().getPrivateKey().toString(16);
//        String publicKey = credentials.getEcKeyPair().getPublicKey().toString(16);
//        System.out.println("private key: " + privateKey+" \n public key: "+ publicKey + " and address: " + accountAddress);
//
//
//
//        //from mnemonic
//
//        String password = null; // no encryption
//        String mnemonic = "candy maple cake sugar pudding cream honey rich smooth crumble sweet treat";
//
//        Credentials credentials2 = WalletUtils.loadBip39Credentials(password, mnemonic);
//        String accountAddress2 = credentials2.getAddress();
//        // Get the unencrypted private key into hexadecimal
//        String privateKey2 = credentials2.getEcKeyPair().getPrivateKey().toString(16);
//        System.out.println("private key2: " + privateKey2 + " and address2: " + accountAddress2);
//
//
//        //create cred file
//        String walletPassword = "secr3t";
//        String walletDirectory = "F:\\work\\blockchain\\workspace\\Ethereum\\test1\\keystore";
//
//        String walletName = WalletUtils.generateNewWalletFile(walletPassword, new File(walletDirectory));
//        System.out.println("wallet location: " + walletDirectory + "/" + walletName);
//
//
//        Credentials credentials3 = WalletUtils.loadCredentials(walletPassword, walletDirectory + "/" + walletName);
//
//        String accountAddress3 = credentials2.getAddress();
//        System.out.println("Account address: " + credentials3.getAddress());
        try {
            String pk = "CHANGE_ME"; // Add a private key here

            // Decrypt and open the wallet into a Credential object
            Credentials credentials4 = WalletUtils.loadCredentials(walletPassword, walletDirectory1 + "/" + walletName1);
            System.out.println("Account address: " + credentials4.getAddress());
            System.out.println("Balance: " + Convert.fromWei(web3.ethGetBalance(credentials4.getAddress(), DefaultBlockParameterName.LATEST).send().getBalance().toString(), Convert.Unit.ETHER));

            // Get the latest nonce
            EthGetTransactionCount ethGetTransactionCount = web3.ethGetTransactionCount(credentials4.getAddress(), DefaultBlockParameterName.LATEST).send();
            BigInteger nonce =  ethGetTransactionCount.getTransactionCount();

            // Recipient address
            String recipientAddress = "0xffcac39cf9120bf7a4c6a13167b9f5ebeb263cfd";

            // Value to transfer (in wei)
            BigInteger value = Convert.toWei("10000", Convert.Unit.ETHER).toBigInteger();

            // Gas Parameters
            BigInteger gasLimit = BigInteger.valueOf(21000);
            BigInteger gasPrice = Convert.toWei("1", Convert.Unit.GWEI).toBigInteger();

            // Prepare the rawTransaction
            RawTransaction rawTransaction  = RawTransaction.createEtherTransaction(
                    nonce,
                    gasPrice,
                    gasLimit,
                    recipientAddress,
                    value);

            // Sign the transaction
            byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials4);
            String hexValue = Numeric.toHexString(signedMessage);

            // Send transaction
            EthSendTransaction ethSendTransaction = web3.ethSendRawTransaction(hexValue).send();
            String transactionHash = ethSendTransaction.getTransactionHash();
            System.out.println("transactionHash: " + transactionHash);

            // Wait for transaction to be mined
            Optional<TransactionReceipt> transactionReceipt;
            do {
                System.out.println("checking if transaction " + transactionHash + " is mined....");
                EthGetTransactionReceipt ethGetTransactionReceiptResp = web3.ethGetTransactionReceipt(transactionHash).send();
                transactionReceipt = ethGetTransactionReceiptResp.getTransactionReceipt();
                Thread.sleep(3000); // Wait 3 sec
            } while(!transactionReceipt.isPresent());

            System.out.println("Transaction " + transactionHash + " was mined in block # " + transactionReceipt.get().getBlockNumber());
            System.out.println("Balance: " + Convert.fromWei(web3.ethGetBalance(credentials4.getAddress(), DefaultBlockParameterName.LATEST).send().getBalance().toString(), Convert.Unit.ETHER));


        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }

}
