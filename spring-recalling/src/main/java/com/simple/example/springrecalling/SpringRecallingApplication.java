package com.simple.example.springrecalling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRecallingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRecallingApplication.class, args);
    }

}
