package com.simple.example.springrecalling.controller;

import com.simple.example.springrecalling.model.Book;
import com.simple.example.springrecalling.model.Greeting;
import com.simple.example.springrecalling.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
public class BookController {
    private BookRepository bookRepository;

    @Autowired
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @ModelAttribute
    public void addAttributes(Model model){
        model.addAttribute("msg", "this is message");
    }
    @GetMapping
    public String getIndex (Model model){
        model.addAttribute("books", bookRepository.findAll());
        return "index";
    }

    @PostMapping
    public String addBook(Book newbook){
        bookRepository.save(newbook);
        return "redirect:/";
    }

    @GetMapping("/greeting")
    public String greetingForm(Model model) {
        model.addAttribute("greeting", new Greeting());
        return "greeting";
    }

    @PostMapping("/greeting")
    public String greetingSubmit(@ModelAttribute Greeting greeting, Model model) {
        model.addAttribute("greeting", greeting);
        return "result";
    }
}
